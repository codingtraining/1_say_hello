package app.com.study.stickyny.sayhello;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText inputNameEdit = (EditText) findViewById(R.id.input_name_edit_text);
        Button enterBtn = (Button) findViewById(R.id.enter_btn);
        final TextView outputText = (TextView) findViewById(R.id.output_text);

        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputMsg = inputNameEdit.getText().toString().trim();
                if (inputMsg.isEmpty()) {
                    Toast.makeText(view.getContext(), R.string.error_msg_name_is_empty, Toast.LENGTH_SHORT).show();
                } else {
                    outputText.setText(makeOutput(inputMsg));
                }
            }
        });
    }
    private String makeOutput(String name) {
        return getString(R.string.hello) + ", " + name + "님. " + getString(R.string.nice);
    }
}
